import { Stage } from "./Stage";

export class Scoreboard extends PIXI.Container {
    private txtScoreTitle:PIXI.Text;
    private txtScore:PIXI.Text;
    private score:number = 0;

    constructor() {
        super();

        this.txtScoreTitle = new PIXI.Text("Score: ", {
            fontFamily: 'Carnevalee Freakshow',
            fontSize: 50,
            align: "right",            
            fill: 0xFFFFFF
        });

        this.txtScore = new PIXI.Text("0", {
            fontFamily: 'Carnevalee Freakshow',
            fontSize: 50,
            align: "right",            
            fill: 0xFFFFFF
        });

        this.txtScore.x = this.txtScoreTitle.x + this.txtScoreTitle.width;

        this.addChild(this.txtScoreTitle);
        this.addChild(this.txtScore);
    }

    setScore(value:number): void {
        this.score = value;
        this.txtScore.text = Math.abs(this.score).toString();
        
        if(value < 0){
            this.txtScore.style.fill = 0xFF0000;
        } else {
            this.txtScore.style.fill = 0xFFFFFF;
        }

        this.x = Stage.width - this.width - 10;
        this.y = 10;
    }

    add(value:number): void {
        this.score += value;
        this.setScore(this.score);
    }

    remove(value:number): void {
        this.score -= value;
        this.setScore(this.score);
    }

    getScore(): number {
        return this.score;
    }
}