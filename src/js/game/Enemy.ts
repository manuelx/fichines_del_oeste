import "pixi.js";
import "pixi-spine";
import * as createjs from "createjs-module";
import { Stage } from "./Stage";
import {Signal} from 'signals';

export class Enemy extends PIXI.Container {
    private static MAX_RESPAWN_TIME:number = 1500;
    private static NUM_LIVES:number = 1;

    life:number = Enemy.NUM_LIVES;
    onDieCompleted:Signal = new Signal();

    shootInterval:number;
    lastShootTime:number = 0;
    bulletColour:number = 0XFF0000;

    isDying:boolean = false;
    character:PIXI.spine.Spine;

    constructor(name?:string){
        super();

        
        this.name = name;
        
        this.refreshShootInterval();

        this.character = new PIXI.spine.Spine(PIXI.loaders.shared.resources.personaje.spineData);
        //this.character.state.setAnimation(0, "idle", true);
        this.addChild(this.character);


        this.hitArea = this.getBounds();
    }

    refreshShootInterval(){
       this.shootInterval = 3000 + Math.random() * 2000;
    }

    appear(): void {
        this.character.skeleton.setToSetupPose();
        this.character.state.tracks = [];

        this.life = Enemy.NUM_LIVES;

        this.alpha = 0;

        createjs.Tween.get(this)
            .to({alpha: 1}, 300);
    }

    shoot(): void {
        this.character.state.setAnimation(0, "shoot", false);
        setTimeout(() => {
            this.character.skeleton.setToSetupPose();
            this.character.state.tracks = [];    
        }, 1500);
    }

    loselives(livesLost:number){
        this.life -= livesLost;
    }

    die(): void {
        if(!this.isDying){
            this.isDying = true;
            this.character.state.setAnimation(0, 'muere', false);
            createjs.Tween.get(this)
                .wait(2000)
                .to({alpha: 0}, 300)
                .call( () => { 
                    this.onDieCompleted.dispatch(); 
                
                    this.respawn();
                });
        }
    }

    respawn(): void {
        let rndTime = Math.random() * Enemy.MAX_RESPAWN_TIME;

        setTimeout(() => {
            this.isDying = false;
            this.appear();
        }, rndTime);
    }
}