import "pixi.js";
import "pixi-spine";
import {Direction, Hero} from './Hero';
import {Enemy} from './Enemy';
import {Bullet} from './Bullet';
import {Background} from './background/Background';
import {Foreground} from './background/Foreground';
import {Scoreboard} from './Scoreboard';
import { MouseInteraction } from "../utils/MouseInteraction";
import { KeyboardInteraction, KeyboardKey } from "../utils/KeyboardInteraction";
import {clone}from 'lodash';


const ENEMY_MAP:Array<{name:string, x:number, y:number}> = [
    {name: "Blinky", x: 671, y: 429},
    {name: "Inky", x: 671, y: 164},
    {name: "Pinky", x: 254, y: 83},
    {name: "Clyde", x: 974, y: 471},
    {name: "Pacman", x: 382, y: 471},
    {name: "Pato", x: 1485, y: 498}
];

export class Stage {
    static width:number;
    static height:number;

    private background:Background;
    private foreground:Foreground;
    private hero:Hero;
    private enemies:Array<Enemy> = new Array<Enemy>();
    private heroBullets:Array<Bullet> = new Array<Bullet>();
    private enemyBullets:Array<Bullet> = new Array<Bullet>();
    private scoreboard:Scoreboard;

    private keyFlags:any = {
        [KeyboardKey.UP_ARROW]: false,
        [KeyboardKey.DOWN_ARROW]: false,
        [KeyboardKey.LEFT_ARROW]: false,
        [KeyboardKey.RIGHT_ARROW]: false,
        [KeyboardKey.SPACE]: false
    };

    //Tiempo para el calculo del tiempo en el superMegaLoop
    private lastUpdateTime:number;

    constructor(private stage:PIXI.Container, private width:number, private height: number){
        Stage.width = this.width;
        Stage.height = this.height;

        this.createLevel();
        
        //Agrego el loop principal
        PIXI.ticker.shared.add(() => {
            this.superMegaLoop()
        });
       
    }

    // Obviamente el super mega loop
    private superMegaLoop(){
        this.calculePositions();
        this.updateEnemiesShoots();
        this.moveHero();
        this.calculateCollisions();
    }

    private calculePositions(): void {
        // Calculo el tiempo que paso desde el ultimo tick
        let previousUpdateTime = this.lastUpdateTime;
        this.lastUpdateTime = (window.performance && performance.now()) || Date.now();
        let cTime         = this.lastUpdateTime - previousUpdateTime;
	    let duration      = 1000; // todas las velocidades estan expresadas en pixels/1000 miliseconds
	     
        
        //hubico al heroe en su nuevo lugar

        //Ubico los bullets en su nueva posicion
        for (let i = 0; i < this.heroBullets.length; i++) {
            let bullet = this.heroBullets[i];
            
            // Calculo cuanto tiene que haber avanzado cada boton
            let _displacementX = bullet.speedX*cTime/duration; 
            let _displacementY = bullet.speedY*cTime/duration; 

            bullet.x += _displacementX;
            bullet.y += _displacementY;
            if ( bullet.y < 0 || bullet.y > this.background.height){
                this.stage.removeChild(bullet);
                this.heroBullets[i] = null;  
            }
        }

         //Ubico los bullets en su nueva posicion
         for (let j = 0; j < this.enemyBullets.length; j++) {
            let bullet = this.enemyBullets[j];
            
            // Calculo cuanto tiene que haber avanzado cada boton
            let _displacementX = bullet.speedX*cTime/duration; 
            let _displacementY = bullet.speedY*cTime/duration; 

            bullet.x += _displacementX;
            bullet.y += _displacementY;
            if ( bullet.y < 0 || bullet.y > this.background.height){
                this.stage.removeChild(bullet);
                this.enemyBullets[j] = null;  
            }
        }

        // remuevo las bullets que estan fuera de la pantalla
        this.heroBullets = this.heroBullets.filter(e => e != null)
        this.enemyBullets = this.enemyBullets.filter(e => e != null)

    
    }

    private calculateCollisions(): void {
        for (let i = 0; i < this.enemies.length; i++) {
            let enemy:Enemy = this.enemies[i];
            let hitArea:PIXI.Rectangle = <PIXI.Rectangle>clone(enemy.hitArea);
            hitArea.x += enemy.x;
            hitArea.y += enemy.y;
            for (let j = 0; j < this.heroBullets.length; j++) {
                let bullet:Bullet = this.heroBullets[j];
                
                if(hitArea.contains(bullet.x, bullet.y) && !enemy.isDying){
                    //le bajo una vida al enemigo
                    enemy.loselives(1);
                    if (enemy.life <= 0){
                        //enemy.onDieCompleted.add( ()=>{this.stage.removeChild(enemy)} )
                        enemy.die();
                        //this.enemies[i] = null;
                    }

                    this.scoreboard.add(1);
                    if(this.scoreboard.getScore() % 10 == 0){
                        this.hero.powerUp();
                    }

                    // destruyo la bala
                    this.stage.removeChild(bullet);
                    this.heroBullets[j] = null;
                }

            }
             // remuevo las bullets que impactaron
            this.heroBullets = this.heroBullets.filter(e => e != null)
            
        }

        //chequeo las bullets de los enemigos
        for (let k = 0; k < this.enemyBullets.length; k++) {
            let bullet:Bullet = this.enemyBullets[k];
            let hitArea:PIXI.Rectangle = <PIXI.Rectangle>clone(this.hero.hitArea);

            hitArea.x += this.hero.x;
            hitArea.y += this.hero.y;

            if(hitArea.contains(bullet.x, bullet.y)){
                this.scoreboard.remove(1);
                // destruyo la bala
                this.stage.removeChild(bullet);
                this.enemyBullets[k] = null;
            }
        }
         // remuevo las bullets que impactaron
         this.enemyBullets = this.enemyBullets.filter(e => e != null)

        // remuevo los enemigos impactados
        this.enemies = this.enemies.filter(e => e != null)
    }

    private updateEnemiesShoots(): void {
        let currentTime:number = (window.performance && performance.now()) || Date.now();

        for (let i = 0; i < this.enemies.length; i++) {
            let enemy = this.enemies[i];
            let cTime = currentTime - enemy.lastShootTime;
            if(cTime > enemy.shootInterval){
                var bullet = this.shoot(enemy.bulletColour, <PIXI.Point>enemy.position, <PIXI.Point>this.hero.position )
                enemy.shoot();
                enemy.lastShootTime = currentTime;
                this.enemyBullets.push(bullet);
                enemy.shootInterval = 3000+ Math.random() * 2000;

            }
        }

        if(this.keyFlags[KeyboardKey.SPACE]){
            let rndEnemy:Enemy = this.enemies[Math.floor(Math.random() * this.enemies.length)];
            let bullet:Bullet = this.shoot(0X0FF0FF, <PIXI.Point>this.hero.position, rndEnemy.position);
            this.heroBullets.push( bullet );
        }
    }


    private moveHero(): void {
        if(this.keyFlags[KeyboardKey.LEFT_ARROW]){
            this.hero.move(Direction.LEFT);
        }
        if(this.keyFlags[KeyboardKey.RIGHT_ARROW]){
            this.hero.move(Direction.RIGHT);
        }
        if(this.keyFlags[KeyboardKey.UP_ARROW]){
            this.hero.jump();
            this.keyFlags[KeyboardKey.UP_ARROW] = false;
        }
    }

    private createLevel(): void {
        MouseInteraction.onClick.add(this.mouseHandler, this);
        // Register keys
        KeyboardInteraction.onKeyUp_UP.add(this.keyboardUpHandler.bind(this, KeyboardKey.UP_ARROW));
        KeyboardInteraction.onKeyUp_DOWN.add(this.keyboardUpHandler.bind(this, KeyboardKey.DOWN_ARROW));
        KeyboardInteraction.onKeyUp_LEFT.add(this.keyboardUpHandler.bind(this, KeyboardKey.LEFT_ARROW));
        KeyboardInteraction.onKeyUp_RIGHT.add(this.keyboardUpHandler.bind(this, KeyboardKey.RIGHT_ARROW));
        KeyboardInteraction.onKeyUp_SPACE.add(this.keyboardUpHandler.bind(this, KeyboardKey.SPACE));

        KeyboardInteraction.onKeyDown_UP.add(this.keyboardDownHandler.bind(this, KeyboardKey.UP_ARROW));
        KeyboardInteraction.onKeyDown_DOWN.add(this.keyboardDownHandler.bind(this, KeyboardKey.DOWN_ARROW));
        KeyboardInteraction.onKeyDown_LEFT.add(this.keyboardDownHandler.bind(this, KeyboardKey.LEFT_ARROW));
        KeyboardInteraction.onKeyDown_RIGHT.add(this.keyboardDownHandler.bind(this, KeyboardKey.RIGHT_ARROW));
        KeyboardInteraction.onKeyDown_SPACE.add(this.keyboardDownHandler.bind(this, KeyboardKey.SPACE));

        // Add background
        this.background = new Background();
        this.stage.addChild(this.background);
        
        // Add score
        this.scoreboard = new Scoreboard();
        this.scoreboard.setScore(0);
        this.stage.addChild(this.scoreboard);
        
        // Add enemies
        for(let i = 0; i < ENEMY_MAP.length; i++){
            let enemyPos:any = ENEMY_MAP[i];
            let enemy:Enemy = new Enemy(enemyPos.name);
            
            enemy.x = enemyPos.x;
            enemy.y = enemyPos.y;

            this.enemies.push(enemy);
            this.stage.addChild(enemy);

            enemy.appear();
        }

        // Add Foreground
        this.foreground = new Foreground();
        this.stage.addChild(this.foreground);
        
        // Add hero
        this.hero = new Hero();
        this.hero.x = Stage.width * .5;
        this.hero.y = Stage.height * .95;
        this.hero.scale.set(.8, .8);
        this.stage.addChild(this.hero);
    }

    private mouseHandler(e:PIXI.interaction.InteractionEvent): void{
        let bullet:Bullet = this.shoot(0X0FF0FF, <PIXI.Point>this.hero.position, new PIXI.Point(e.data.global.x, e.data.global.y));
        this.heroBullets.push( bullet );
    }

    private shoot(color:number, origin:PIXI.Point, target:PIXI.Point): Bullet {
        let bullet = new Bullet(color, origin, target);
        this.stage.addChild( bullet );
        return bullet;
    }

    private keyboardUpHandler(keyCode:KeyboardKey): void {
        this.keyFlags[keyCode] = false;
    }

    private keyboardDownHandler(keyCode:KeyboardKey): void {
        this.keyFlags[keyCode] = true;
    }
}