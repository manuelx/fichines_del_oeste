import * as PIXI from 'pixi.js';
import { Stage } from './Stage';
import { MouseInteraction } from './../utils/MouseInteraction';
import { KeyboardInteraction } from '../utils/KeyboardInteraction';

type Dimensions = {
    width:number,
    height:number
};

export default class Game {
    private dimensions: Dimensions = {
        width: 1600,
        height: 768
    };
    private app: PIXI.Application;
    private stage: Stage;

    constructor() {
        this.app = new PIXI.Application({
            width: this.dimensions.width,
            height: this.dimensions.height,
            antialias: true,
            transparent: false,
            resolution: 1
    	});


        this.app.renderer.autoResize = true;

        document.body.appendChild(this.app.view);
        
        window.addEventListener("resize", this.resize.bind(this));
        this.resize();
    }

    init(): void {
        this.app.ticker.add(() => {
            //console.log("rendering");
        });
        
        new KeyboardInteraction();
        new MouseInteraction(this.app.stage);

        let loader:PIXI.loaders.Loader= PIXI.loaders.shared;
        let spineLoaderOptions:any = { metadata: { spineAtlasFile: 'assets/images/skeletons.atlas' } };
       
        loader.add("spineboy", "assets/images/pixie.json");
        loader.add("bg", "assets/images/bg.json", spineLoaderOptions);
        loader.add("bg2", "assets/images/bg2.json", spineLoaderOptions);
        loader.add("personaje", "assets/images/personaje1.json", spineLoaderOptions);
       
        loader.load(() => {
            this.stage = new Stage(this.app.stage, this.dimensions.width, this.dimensions.height);
        });
    }

    private resize(): void {
        let scale = window.innerHeight / this.dimensions.height;
        this.app.stage.scale.set(scale, scale);
        if(this.app.stage.width > window.innerWidth){
            scale = window.innerWidth / this.dimensions.width;
            this.app.stage.scale.set(scale, scale);
        }
    }
}