import "pixi.js";

export class Background extends PIXI.Container {
    constructor() {
        super();

        this.name = "background";

        this.addBackground();
    }
    
    private addBackground(): void {
        let background:PIXI.spine.Spine = new PIXI.spine.Spine(PIXI.loaders.shared.resources.bg.spineData);

        background.state.setAnimation(0, "animation", true);

        this.addChild(background);
    }

}