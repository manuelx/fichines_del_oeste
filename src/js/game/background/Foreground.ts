import "pixi.js";

export class Foreground extends PIXI.Container {
    constructor() {
        super();

        this.name = "foreground";

        this.addForeground();
    }
    
    private addForeground(): void {
        let foreground:PIXI.spine.Spine = new PIXI.spine.Spine(PIXI.loaders.shared.resources.bg2.spineData);

        this.addChild(foreground);
    }

}