const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let outputPath = 'dist';

module.exports = {
    entry: './src/js/app.ts',
    output: {
        path: path.resolve(__dirname, outputPath),
        filename: "bundle.js",
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    devServer: {
        contentBase: './dist'
    },
    devtool: 'inline-source-maps',
    module: {
        rules: [{
                test: /\.ts?$/,
                exclude: /node_modules/,
                use: [
                    { loader: "ts-loader" }
                ]
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.ttf$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'file-loader' }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(outputPath),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new webpack.ProvidePlugin({
            PIXI: 'pixi.js',
            $: 'jquery'
        }),
        new CopyWebpackPlugin([
            { context: 'src/assets', from: '**/*', to: './assets/' }
        ])
    ]
}